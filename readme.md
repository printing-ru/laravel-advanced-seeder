# Laravel Advanced Seeder
Includes abstract classes to help seed your data.

### Instalation
```sh
    composer require navinlab/laravel-advanced-seeder
```

### Usage
Extend you seed from classes bellow:
* ArraySeeder - export data from array
* JsonSeeder - export data from json
* ConfigSeeder - export data from config
* CsvSeeder - export data from csv (field names take from first row)

__To add support timestamps fields, add TimestampSeeder trait__ to fill these fields auto.

### Examples
* ArraySeeder
```php
use Illuminate\Support\Collection;
use NavinLab\LaravelAdvancedSeeder\ArraySeeder;
use NavinLab\LaravelAdvancedSeeder\Traits\TimestampSeeder;

class UsersTableSeeder extends ArraySeeder
{
    use TimestampSeeder;
    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return \App\Models\User::class;
    }

    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    public function getData()
    {
        return new Collection([
            [
                'firstname' => 'Admin',
                'lastname' => 'Test',
                'email' => 'admin@localhost',
                'password' => '***',
            ]
        ]);
    }
}

```
* ConfigSeeder
```php
use NavinLab\LaravelAdvancedSeeder\ConfigSeeder;
use NavinLab\LaravelAdvancedSeeder\Traits\TimestampSeeder;

class UsersTableSeeder extends ConfigSeeder
{
    use TimestampSeeder;
    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return \App\Models\User::class;
    }

    /**
     * Get the config path
     * @return string
     */
    protected function getConfigPath()
    {
        return 'seeds.users';
    }
}
```
__config/seeds.php__
```php
return [
    /**
     * Users data to init project
     */
    'users' => [
        /**
         * Main USER
         */
        [
            /**
             * First name
             */
            'firstname' => env('SMU_FIRSTNAME', ''),
            /**
             * Email
             *
             * By default includes admin@host from app.url configuration value
             */
            'email' => env('SMU_EMAIL', sprintf('admin@%s', parse_url(config('app.url'), PHP_URL_HOST))),
            /**
             * Last name
             */
            'lastname' => env('SMU_LAST_NAME', ''),
            /**
             * Password
             */
            'password' => env('SMU_PASSWORD', 'define'),
        ],
    ],
];
```
* JsonSeeder
```php
use NavinLab\LaravelAdvancedSeeder\JsonSeeder;
use NavinLab\LaravelAdvancedSeeder\Traits\TimestampSeeder;

class UsersTableSeeder extends JsonSeeder
{
    use TimestampSeeder;
    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return \App\Models\User::class;
    }

    /**
     * The source file path
     *
     * @return string
     */
    protected function getSourcePath()
    {
        return resource_path('seeds/users.json');
    }
}
```
__seeds/users.json__
```json
[
  {
    "firstname": "Admin",
    "lastname": "Test",
    "email": "admin@localhost",
    "password": "***"
  }
]
```
* CsvSeeder
```php
use NavinLab\LaravelAdvancedSeeder\CsvSeeder;
use NavinLab\LaravelAdvancedSeeder\Traits\TimestampSeeder;

class UsersTableSeeder extends CsvSeeder
{
    use TimestampSeeder;
    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return \App\Models\User::class;
    }

    /**
     * The source file path
     *
     * @return string
     */
    protected function getSourcePath()
    {
        return resource_path('seeds/users.csv');
    }
}
```
__seeds/users.csv__
```csv
firstname,lastname,email,password
Admin,Test,admin@localhost,***
```
* CsvSeeder advanced
```php
use NavinLab\LaravelAdvancedSeeder\CsvSeeder;
use NavinLab\LaravelAdvancedSeeder\Traits\TimestampSeeder;

class UsersTableSeeder extends CsvSeeder
{
    use TimestampSeeder;
    
    protected $delimiter = ';';
    protected $enclosure = '"';
    
    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return \App\Models\User::class;
    }
    
    /**
     * The source file path
     *
     * @return string
     */
    protected function getSourcePath()
    {
        return resource_path('seeds/users.csv');
    }
    /**
     * Get the header codes collection
     *
     * @param Collection $collection
     * @return Collection
     */
    protected function getHeader(Collection $collection)
    {
        return new Collection(
            ['firstname', 'password', 'email', 'lastname']
        );
    }
}
```
__seeds/users.csv__ (no header)
```csv
"Admin";"define";"admin@localhost";"Test"
```
### Development
Want to contribute? Great! Be free to make a pull request.

### Authors
  * Anton Pavlov <anton.pavlov.it@gmail.com>
  