<?php
/**
 * laravel-advanced-seeder.
 * Date: 30/04/17
 * Time: 07:53
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelAdvancedSeeder;


use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

abstract class ArraySeeder extends Seeder
{
    protected $chunkSize = 100;
    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    abstract public function getData();

    /**
     * Get model class
     *
     * @return mixed
     */
    abstract public function getModelClass();

    /**
     * Map additional dynamic data
     *
     * @param Collection $collection
     * @return Collection
     */
    protected function map(Collection $collection) {
        return $collection;
    }
    /**
     * Run the database seeds
     */
    public function run() {
        $data = $this->map($this->getData());
        $className = $this->getModelClass();
        /** @var Collection $chunk */
        foreach ($data->chunk($this->chunkSize) as $chunk) {
            $className::insert($chunk->toArray());
        }
    }
}