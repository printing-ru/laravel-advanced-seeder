<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 08:35
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelAdvancedSeeder;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;

abstract class ConfigSeeder extends ArraySeeder
{
    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    public function getData()
    {
        return new Collection(
            Config::get($this->getConfigPath(), [])
        );
    }

    /**
     * Get the config path
     * @return string
     */
    abstract protected function getConfigPath();
}