<?php
/**
 * laravel-advanced-seeder.
 * Date: 30/04/17
 * Time: 07:43
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelAdvancedSeeder;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

abstract class CsvSeeder extends SourceSeeder
{
    /**
     * Default csv delimiter
     *
     * @var string
     */
    protected $delimiter = ',';
    /**
     * Default csv enclosure
     *
     * @var string
     */
    protected $enclosure = '"';
    /**
     * Default csv escape
     *
     * @var string
     */
    protected $escape = "\\";

    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    public function getData()
    {
        $collection = (new Collection(
                explode(PHP_EOL, File::get($this->getSourcePath()))
            ))
            ->map(function ($line) {
                return str_getcsv($line, $this->delimiter, $this->enclosure, $this->escape);
            });
        $header = $this->getHeader($collection);
        return $collection->map(function ($item) use($header) {
            return $header->combine($item)->toArray();
        });

    }

    /**
     * Get the header codes collection
     *
     * @param Collection $collection
     * @return Collection
     */
    protected function getHeader(Collection $collection)
    {
        return new Collection(
            $collection->shift()
        );
    }
}