<?php
/**
 * laravel-advanced-seeder
 * Date: 30/04/17
 * Time: 07:36
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelAdvancedSeeder;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

abstract class JsonSeeder extends SourceSeeder
{
    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    public function getData()
    {
        return new Collection(
            json_decode(File::get($this->getSourcePath()), true)
        );
    }
}