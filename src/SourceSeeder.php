<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 08:36
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelAdvancedSeeder;


abstract class SourceSeeder extends ArraySeeder
{
    /**
     * The source file path
     *
     * @return string
     */
    abstract protected function getSourcePath();
}