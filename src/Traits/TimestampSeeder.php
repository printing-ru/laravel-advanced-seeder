<?php
namespace NavinLab\LaravelAdvancedSeeder\Traits;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * laravel-advanced-seeder.
 * Date: 30/04/17
 * Time: 10:18
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */
trait TimestampSeeder
{
    protected function map(Collection $collection) {
        return $collection->map(function ($item) {
            $item['created_at'] = Carbon::now();
            $item['updated_at'] = Carbon::now();
            return $item;
        });
    }
}