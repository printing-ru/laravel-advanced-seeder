<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:14
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Tests\Cases\ArraySeeder;
use Tests\Models\TestModel;

class ArraySeederTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        TestModel::$callsChunks = [];
        TestModel::$callsCount = 0;
    }

    protected function getData($itemsCount = 5) {
        return Collection::times($itemsCount, function ($number) {
            return [
                'id' => $number,
                'name' => 'Test',
            ];
        });
    }

    public function testRunUseChunks() {
        $itemsCount = rand(5, 20);
        $chunkSize = rand(2, $itemsCount);

        //prepare test data
        $collection = $this->getData($itemsCount);

        //run logic
        (new ArraySeeder($collection, $chunkSize))->run();

        $this->assertEquals(ceil($itemsCount/$chunkSize), TestModel::$callsCount);

        $lastChunk = array_pop(TestModel::$callsChunks);
        foreach (TestModel::$callsChunks as $chunk) {
            $this->assertCount($chunkSize, $chunk);
        }
        //check last one
        $this->assertLessThanOrEqual($chunkSize, count($lastChunk));
    }
}