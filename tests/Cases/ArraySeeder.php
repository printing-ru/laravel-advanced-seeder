<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:01
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Cases;


use Illuminate\Support\Collection;
use Tests\Models\TestModel;

class ArraySeeder extends \NavinLab\LaravelAdvancedSeeder\ArraySeeder
{

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * ArraySeeder constructor.
     * @param Collection $collection
     * @param int $chunkSize
     */
    function __construct(Collection $collection, $chunkSize = 1)
    {
        $this->collection = $collection;
        $this->chunkSize = $chunkSize;
    }

    /**
     * Get source data
     * Should contain array of items to insert into database
     *
     * @return Collection
     */
    public function getData()
    {
        return $this->collection;
    }

    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return TestModel::class;
    }
}