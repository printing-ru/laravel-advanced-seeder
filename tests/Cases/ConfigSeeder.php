<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:11
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Cases;


use Tests\Models\TestModel;

class ConfigSeeder extends \NavinLab\LaravelAdvancedSeeder\ConfigSeeder
{

    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return TestModel::class;
    }

    /**
     * Get the config path
     * @return string
     */
    protected function getConfigPath()
    {
        return 'seed.test';
    }
}