<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:13
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Cases;


use Tests\Models\TestModel;

class JsonSeeder extends \NavinLab\LaravelAdvancedSeeder\JsonSeeder
{

    /**
     * Get model class
     *
     * @return mixed
     */
    public function getModelClass()
    {
        return TestModel::class;
    }

    /**
     * The source file path
     *
     * @return string
     */
    protected function getSourcePath()
    {
        return 'testpath';
    }
}