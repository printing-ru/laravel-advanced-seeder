<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:14
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use \Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Cases\ConfigSeeder;
use Tests\Models\TestModel;

class ConfigSeederTest extends TestCase
{
    protected $config;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->config = m::mock(sprintf('alias:%s', Config::class));
        TestModel::$callsChunks = [];
        TestModel::$callsCount = 0;
    }

    /**
     * Test method getData
     */
    public function testGetData() {
        $this->config->shouldReceive('get')
            ->once()
            ->andReturn(new Collection([
                $data = ['id' => 1],
            ]));

        //run logic
        (new ConfigSeeder())->run();

        $this->assertCount(1, TestModel::$callsChunks);
        $this->assertArraySubset($data, TestModel::$callsChunks[0][0]);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}