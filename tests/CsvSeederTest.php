<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:14
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests;

use Illuminate\Support\Facades\File;
use \Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Cases\CsvSeeder;
use Tests\Models\TestModel;

class CsvSeederTest extends TestCase
{
    protected $file;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->file = m::mock(sprintf('alias:%s', File::class));
        TestModel::$callsChunks = [];
        TestModel::$callsCount = 0;
    }

    /**
     * Test method getData
     */
    public function testGetDataAndGetHeader() {
        $this->file->shouldReceive('get')
            ->once()
            ->andReturn(sprintf('id%s1', PHP_EOL));

        //run logic
        (new CsvSeeder())->run();

        $this->assertCount(1, TestModel::$callsChunks);
        $this->assertArraySubset(['id' => 1], TestModel::$callsChunks[0][0]);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}