<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:14
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests;

use Illuminate\Support\Facades\File;
use \Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Cases\JsonSeeder;
use Tests\Models\TestModel;

class JsonSeederTest extends TestCase
{
    protected $file;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->file = m::mock(sprintf('alias:%s', File::class));
        TestModel::$callsChunks = [];
        TestModel::$callsCount = 0;
    }

    /**
     * Test method getData
     */
    public function testGetData() {
        $data = [
            ['id' => 1],
        ];
        $this->file->shouldReceive('get')
            ->once()
            ->andReturn(json_encode($data));

        //run logic
        (new JsonSeeder())->run();

        $this->assertCount(1, TestModel::$callsChunks);
        $this->assertArraySubset($data, TestModel::$callsChunks[0]);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}