<?php
/**
 * laravel-database-seeder.
 * Date: 30/04/17
 * Time: 09:04
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Models;


class TestModel
{
    /**
     * Store the calls count
     *
     * @var int
     */
    public static $callsCount = 0;
    /**
     * Store the calls data
     *
     * @var array
     */
    public static $callsChunks = [];

    /**
     * Emulate insert
     *
     * @param $data
     */
    static public function insert($data) {
        self::$callsChunks[] = $data;
        self::$callsCount++;
    }
}